FROM golang:1.11.2 as builder
RUN go get -u github.com/golang/dep/cmd/dep
WORKDIR /go/src/app
COPY . .
RUN dep ensure
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine
EXPOSE 8080
COPY --from=builder /go/src/app/app /app
ENTRYPOINT ["/app"]
